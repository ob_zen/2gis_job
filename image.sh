#!/bin/bash
#Скрипт для конвертации размера изображений

#Если внезапно передали больше аргументов - то выходим
if [ "$#" -gt 1 ]
then
  echo "Wrong usage. Please use -h option for help"
  exit
fi

function compress_image {
  #Функция для сжатия по длинной стороне
  #Оцениваем изначальные размеры картинки
  l1=`identify -format "%wx%h" "$1" | awk -F "x" '{print $1}'`
  l2=`identify -format "%wx%h" "$1" | awk -F "x" '{print $2}'`
  if [ "$l1" -ge 360 ] || [ "$l2" -ge 360 ]
  #Если обе стороны меньше 360px то сжимать уже ничего не нужно
  then
    #Уменьшаем до 360px длинную сторону
    if [ "$l1" -gt "$l2" ]
    then
      l1=360
    else
      l2=360
    fi
    #Если это уже сжатая по одной стороне картинка
    #То изменять имя не нужно
    if [[ "$1" == *"_thumbnail.jpg" ]]
    then
    #Сжимаем изображение
      convert -resize "$l1"x"$l2"! "$1" "$1"
    else
      #Переименовываем файл, если сжимаем его в первый раз
      name=`echo "$1" | awk -F "." '{print $1}'`_thumbnail.jpg
      convert -resize "$l1"x"$l2"! "$1" "$name"
    fi
  fi
}

if [ -n "$1" ]
#Проверяем наличие аргумента, и его текст
#По правилам хорошего тона для -h делаем мелкую хэлпу
then
  case $1 in
    "HELP" | "help" | "-h" )
      echo "Usage: convert_image.sh [FILE]"
      echo "File name required full path."
    ;;
    #Если не спросили хэлпу, то значит передали имя файла
    #Я предполагаю, что передали АБСОЛЮТНЫЙ ПУТЬ
    #либо файл лежит в этой же папке
    * )
      #Проверяем наличие файла со списком для сжатия
      if [ -f "$1" ]
      then
        #Получаем список файлов для сжатия из файла по маске
        list_of_convert_images=`cat "$1" | grep ".jpg$"`
        #В полученном массиве циклом перебираем файлы
        for i in ${list_of_convert_images[@]}
        do
          #Проверка наличия файла
          if [ -f "$i" ]
          then
            name=`echo "$i" | awk -F "." '{print $1}'`_thumbnail
            res=`ls . | grep "$name"`
          #Грепаем содержимое папки по по имя_thumbnail
          #Если такой файл есть, значит оригинал был сжат и второй раз сжимать не надо
            if [ -z "$res" ]
            then
              #Вызов функции со сжатием
              compress_image "$i"
            fi
          else
            echo "File $i does not exist"
          fi
        done  
      else
        #*Жирные вздохи сожаления о том, что файла со списком имен нет
        echo "Sorry file $1 not found. Please enter name of exist file"
      fi
    ;;
  esac  
else
  #Если скрипт запустили без параметров - то какбэ сжимаем что есть в директории со скриптом
  #Получаем список файлов в директории по маске
  list_of_convert_images=`ls . | grep ".jpg$"`
  #Бежим циклом по полученному массиву файлов
  for i in ${list_of_convert_images[@]}
  do
    echo "$i"
    #Грепеам по маске имя_thumbnail
    name=`echo "$i" | awk -F "." '{print $1}'`_thumbnail
    res=`ls . | grep "$name"`
    #Если такой файл есть, значит оригинал был сжат и второй раз сжимать не надо
    if [ -z "$res" ]
    then
      #Вызов функции со сжатием
      compress_image "$i"
    fi
  done 
fi
